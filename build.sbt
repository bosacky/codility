name := "Codility Challenges"
version := "0.1"
scalaVersion := "2.12.17"

libraryDependencies ++= Seq(
  "org.scalacheck" %% "scalacheck" % "1.17.0" % "test",
  "org.scalatest" %% "scalatest" % "3.2.14" % "test"
)