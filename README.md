# Codility Challenges
Here are some of my solutions for the past [Codility Challenges](https://app.codility.com/programmers/challenges/)


## Zirconium 2019
- [Code](src/main/scala/DreamTeam.scala)
- [Tests](src/test/scala/DreamTeamTest.scala)
- [Golden award](https://app.codility.com/cert/view/certKQF7FU-VM37GUD45A4YDWEJ/) 
- [Detailed assessment](https://app.codility.com/cert/view/certKQF7FU-VM37GUD45A4YDWEJ/details/)

## Silver 2020
- [Code](src/main/scala/RectangleStrip.scala)
- [Tests](src/test/scala/RectangleStripTest.scala)
- [Golden award](https://app.codility.com/cert/view/certP5CAJM-FAHWNNQWEEKQDWNZ/)
- [Detailed assessment](https://app.codility.com/cert/view/certP5CAJM-FAHWNNQWEEKQDWNZ/details/)

## The Doge 2021
- [Code](src/main/scala/PetsAndToys.scala)
- [Tests](src/test/scala/PetsAndToysTest.scala)
- [Golden award](https://app.codility.com/cert/view/certTGKZW6-M4UUFAMRPDWZA86P/)
- [Detailed assessment](https://app.codility.com/cert/view/certTGKZW6-M4UUFAMRPDWZA86P/details/)

## National Coding Week Challenge 2021
- [Code](src/main/scala/LargestString.scala)
- [Tests](src/test/scala/LargestStringTest.scala)
- [Golden award](https://app.codility.com/cert/view/certQRD2QB-XZGS3CBYCQQ6BKVK/)
- [Detailed assessment](https://app.codility.com/cert/view/certQRD2QB-XZGS3CBYCQQ6BKVK/details/)

## Spooktober 2021
- [Code](src/main/scala/StackOfCoins.scala)
- [Tests](src/test/scala/StackOfCoinsTest.scala)
- [Golden award](https://app.codility.com/cert/view/certDUVJEY-CSPB4ES9Q45KS44V/)
- [Detailed assessment](https://app.codility.com/cert/view/certDUVJEY-CSPB4ES9Q45KS44V/details/)

## Year of the Tiger 2022
- [Code](src/main/scala/TricoloredTowers.scala)
- [Tests](src/test/scala/TricoloredTowersTest.scala)
- [Golden award](https://app.codility.com/cert/view/certX954GT-B27PKXWX669BN8GK/)
- [Detailed assessment](https://app.codility.com/cert/view/certX954GT-B27PKXWX669BN8GK/details/)

## Fury Road 2022
- [Code](src/main/scala/ScooterRoad.scala)
- [Tests](src/test/scala/ScooterRoadTest.scala)
- [Golden award](https://app.codility.com/cert/view/certENP5RF-62ANVSJHYHRCHMP7/)
- [Detailed assessment](https://app.codility.com/cert/view/certENP5RF-62ANVSJHYHRCHMP7/details/)

## Jurassic Code 2022
- [Code](src/main/scala/LargestBalancedRadius.scala)
- [Tests](src/test/scala/LargestBalancedRadiusTest.scala)
- [Golden award](https://app.codility.com/cert/view/certKNJ75N-BN6T23EB2M263ANS/)
- [Detailed assessment](https://app.codility.com/cert/view/certKNJ75N-BN6T23EB2M263ANS/details/)

## National Coding Week 2022
- [Code](src/main/scala/OddNetwork.scala)
- [Tests](src/test/scala/OddNetworkTest.scala)
- [Golden award](https://app.codility.com/cert/view/certUAXEDY-GVFA2ZRB4T83B4X7/)
- [Detailed assessment](https://app.codility.com/cert/view/certUAXEDY-GVFA2ZRB4T83B4X7/details/)

## Carol of the Code 2022
- [Code](src/main/scala/PrettyTiling.scala)
- [Tests](src/test/scala/PrettyTilingTest.scala)
- [Golden award](https://app.codility.com/cert/view/certPR2823-X7HZZ6SJK3MPCCB9/)
- [Detailed assessment](https://app.codility.com/cert/view/certPR2823-X7HZZ6SJK3MPCCB9/details/)

## Year of the Rabbit 2022
- [Code](src/main/scala/Dinner.scala)
- [Tests](src/test/scala/DinnerTest.scala)
- [Golden award](https://app.codility.com/cert/view/certA2HQQK-PJAGFGVK9Q6QH4RX/)
- [Detailed assessment](https://app.codility.com/cert/view/certA2HQQK-PJAGFGVK9Q6QH4RX/details/)

## Pi Code 2022
- [Code](src/main/scala/LetterCover.scala)
- [Tests](src/test/scala/LetterCoverTest.scala)
- [Silver award](https://app.codility.com/cert/view/certP7SND7-44EX3KUP79DQEQ9N/)
- [Detailed assessment](https://app.codility.com/cert/view/certP7SND7-44EX3KUP79DQEQ9N/details/)
