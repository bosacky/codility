object LargestString {
  def solution(s: String): String = {
    val chars = s.toCharArray
    var i = 0
    while (i < chars.length) {
      val str = chars.slice(i - 2, i + 1).mkString

      if (str.equals("abb")) {
        chars(i - 2) = 'b'
        chars(i - 1) = 'a'
        chars(i) = 'a'
        if (i > 3 && chars(i - 4) == 'a' && chars(i - 3) == 'b') {
          i = i - 4
        }
      }
      i += 1
    }
    chars.mkString
  }
}
