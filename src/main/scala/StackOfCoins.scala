object StackOfCoins {
  def solution(a: Array[Int]): Int = {
    val forth = Array.ofDim[Int](a.length)
    val back = Array.ofDim[Int](a.length)
    val summed = Array.ofDim[Int](a.length)

    var prevLeft = 0
    var prevRight = 0
    val maxIdx = a.length - 1
    var max = 0

    for (i <- a.indices) {
      val left = a(i)
      val right = a(maxIdx - i)

      forth(i) = prevLeft / 2 + left
      prevLeft = forth(i)
      back(maxIdx - i) = prevRight / 2 + right
      prevRight = back(maxIdx - i)

      summed(i) += forth(i) - a(i)
      summed(maxIdx - i) += back(maxIdx - i)

      max = math.max(math.max(summed(i), summed(maxIdx - i)), max)
    }

    max
  }

}
