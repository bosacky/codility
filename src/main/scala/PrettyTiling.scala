/** N is an integer within the range [1..100,000]; string representing a tile
  * has 4 letters (exactly one occurrence of 'R', 'G', 'B' and 'W').
  */
object PrettyTiling {

  sealed abstract class Color(val label: Char)
  object Color {
    final case object R extends Color('R')
    final case object G extends Color('G')
    final case object B extends Color('B')
    final case object W extends Color('W')

    def of(c: Char): Color = c match {
      case 'R' => R
      case 'G' => G
      case 'B' => B
      case 'W' => W
    }
  }

  case class Tile(top: Color, right: Color, bottom: Color, left: Color) {
    lazy val rotate: List[(Tile, Int)] =
      List(
        (this, 0),
        (Tile(left, top, right, bottom), 1),
        (Tile(right, bottom, left, top), 1),
        (Tile(bottom, left, top, right), 2)
      )
  }

  object Tile {
    def of(colors: String): Tile = {
      val Seq(top, right, bottom, left) = colors.map(Color.of)
      Tile(top, right, bottom, left)
    }
  }

  def solution(a: Array[String]): Int = {
    val tiles = a.map(Tile.of)
    val first = tiles.head
    val allCosts = tiles.tail
      .foldLeft(first.rotate) { (b, a) =>
        val rotations = a.rotate
        b.flatMap { case (previous, total) =>
          rotations
            .find(_._1.left == previous.right)
            .map { case (next, cost) =>
              (next, cost + total)
            }
        }
      }
      .map(_._2)
    allCosts.min
  }
}
