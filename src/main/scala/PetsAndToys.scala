import scala.annotation.tailrec
import scala.collection.mutable

object PetsAndToys {
  class UnionFind(n: Int) {
    private val parents: mutable.Map[Int, Int] = mutable.Map((0 until n).map(i => i -> i): _*).result()

    def find(x: Int): Int = {
      @tailrec
      def find(x: Int, originalValue: Int): Int = {
        val parent = parents(x)
        if (parent != x) {
          find(parent, originalValue)
        }
        else {
          //Path compression
          parents(originalValue) = x
          x
        }
      }

      find(x, x)
    }

    def union(xy: (Int, Int)): Unit = union(xy._1, xy._2)

    def union(x: Int, y: Int): Unit = {
      val parentOfX = find(x)
      val parentOfY = find(y)
      //Order by rank
      if (parentOfX < parentOfY)
        parents.update(parentOfX, parentOfY)
      else
        parents.update(parentOfY, parentOfX)
    }
  }


  def solution(pets: Array[Int], toys: Array[Int], relationFrom: Array[Int], relationTo: Array[Int]): Boolean = {
    val n = pets.length
    val unionFind = relationFrom.zip(relationTo)
      .foldLeft(new UnionFind(n))((uf, xy) => {
        uf.union(xy)
        uf
      })

    val petsAndToysMappedSets = pets.zip(toys).zipWithIndex.foldLeft(Map[Int, (Map[Int, Int], Map[Int, Int])]())((map, petAndToyWithIndex) => {
      val pet = petAndToyWithIndex._1._1
      val toy = petAndToyWithIndex._1._2
      val idx = petAndToyWithIndex._2
      val union = unionFind.find(idx)

      val originalValue = map.get(union)
      val newValue = if (originalValue.isDefined) {
        val petsMap = originalValue.get._1.updated(pet, originalValue.get._1.getOrElse(pet, 0) + 1)
        val toysMap = originalValue.get._2.updated(toy, originalValue.get._2.getOrElse(toy, 0) + 1)
        (petsMap, toysMap)
      } else {
        ((Map(pet -> 1), Map(toy -> 1)))
      }
      map.updated(union, newValue)
    })

    !petsAndToysMappedSets.values.exists(petsAndToysMappedSet => {
      val leftMap = petsAndToysMappedSet._1
      val rightMap = petsAndToysMappedSet._2
      val diff = (leftMap.toSet diff rightMap.toSet).toMap

      diff.nonEmpty
    })
  }
}
