object DreamTeam {
  def solution(frontEnds: Array[Int], backEnds: Array[Int], frontEndCount: Int): Int = {
    validateInput(frontEnds, backEnds, frontEndCount)
    if (frontEndCount == 0) return backEnds.sum

    val queue = collection.mutable.PriorityQueue[(Int, Int)]()(Ordering.by[(Int, Int), Int](_._1).reverse)

    var backEndSum = 0
    var frontEndSum = 0

    for (i <- frontEnds.indices) {
      val diff = frontEnds(i) - backEnds(i)
      if (queue.size < frontEndCount || queue.head._1 < diff) {
        if (queue.size == frontEndCount) {
          val dequeued = queue.dequeue()
          frontEndSum = frontEndSum - frontEnds(dequeued._2)
          backEndSum = backEndSum + backEnds(dequeued._2)
        }
        queue.enqueue((diff, i))
        frontEndSum = frontEndSum + frontEnds(i)
      } else {
        backEndSum = backEndSum + backEnds(i)
      }
    }

    backEndSum + frontEndSum
  }

  private def validateInput(frontEndContributions: Array[Int], backEndContributions: Array[Int], frontEndCount: Int): Unit = {
    if (frontEndContributions.length != backEndContributions.length) throw new IllegalArgumentException("FrontEnd and BackEnd contributions must have the same array size.")
    if (frontEndCount > frontEndContributions.length) throw new IllegalArgumentException("FrontEnd developers count must not exceed the array size.")
  }

  def functional(a: Array[Int], b: Array[Int], f: Int): Int = {
    val diff = a.zip(b).zipWithIndex.map(ab => (ab._1._1, ab._1._2, ab._1._1 - ab._1._2, ab._2))
      .sortBy(_._3)(Ordering.Int.reverse).take(f)
    val idxs = diff.map(_._4)
    val sumB = b.zipWithIndex.filter(bi => !idxs.contains(bi._2)).map(_._1).sum
    val sumA = diff.map(_._1).sum

    sumA + sumB
  }
}
