object RectangleStrip {
  def solution(a: Array[Int], b: Array[Int]): Int = {
    if (a.length != b.length) throw new IllegalArgumentException("A and B array sizes must be equal")
    a.zip(b).flatMap(_.productIterator.toSet).groupBy(identity).values.map(_.length).max
  }
}
