object Dinner {

  def solution(as: Array[Int], bs: Array[Int]): Int = {
    import scala.annotation.tailrec
    @tailrec
    def minRotation(as: Seq[Int], bs: Seq[Int], rotation: Int): Int = {
      if (rotation == as.length) -1
      else {
        if (as.zip(bs).exists { case (a, b) => a == b }) minRotation(as.tail :+ as.head, bs, rotation + 1)
        else rotation
      }
    }
    minRotation(as, bs, 0)
  }
}
