object LargestBalancedRadius {
  def dist(x: Int, y: Int) = math.sqrt(math.pow(x, 2) + math.pow(y, 2))

  def solution(xs: Array[Int], ys: Array[Int], colors: String): Int = {
    val sortedByDistance = xs.zip(ys).zip(colors)
      .map({ case ((a, b), c) => (a, b, c) }) //just flattens the zipped stuff
      .map(v => (dist(v._1, v._2), v._3))
      .sortBy(_._1)

    var maxCircle = 0
    var totalRedCount = 0
    var totalGreenCount = 0

    for (i <- sortedByDistance.indices) {
      val (dist, color) = sortedByDistance(i)
      val isLastWithDist = (i == sortedByDistance.length - 1) || sortedByDistance(i + 1)._1 != dist

      if (color == 'R') totalRedCount += 1
      else totalGreenCount += 1

      if (isLastWithDist) {
        if (totalGreenCount == totalRedCount) maxCircle = totalGreenCount << 1
      }
    }

    maxCircle
  }
}
