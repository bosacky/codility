import scala.collection.mutable

object TricoloredTowers {
  def solution(input: Array[String]): Int = {
    var max = 0
    val map = mutable.Map[String, Int]()

    for (str <- input) {
      val alts = Set(
        str,
        Array(str(0), str(2), str(1)).mkString,
        Array(str(1), str(0), str(2)).mkString)

      alts.foreach(a => {
        val count = map.get(a)
        val newCount = count.map(_ + 1).getOrElse(1)
        max = math.max(max, newCount)
        map.update(a, newCount)
      })
    }

    max
  }
}
