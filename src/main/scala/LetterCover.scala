object LetterCover {
  //This is correct but fails big dense, big random and big long performance tests
  def solution(p: String, q: String): Int = {
    val length = p.length
    val queue  = collection.mutable.PriorityQueue[(Int, Int, Set[Char])]()(Ordering.by(_._1)).reverse
    queue.enqueue((0, 0, Set[Char]()))
    var res         = -1

    while (res < 0 && queue.nonEmpty) {
      val (cost, idx, set) = queue.dequeue()

      if (idx == length) {
        res = set.size
      } else {
        queue.enqueue((cost + (if (set.contains(p(idx))) 0 else 1), idx + 1, set + p(idx)))
        queue.enqueue((cost + (if (set.contains(q(idx))) 0 else 1), idx + 1, set + q(idx)))
      }
    }
    res
  }

  //This is correct but fails all the performance tests
  def bruteForce(p: String, q: String): Int = {
    val length = p.length;
    def distinctLetters(i: Int, res: String): Int = {
      if (i < length) {
        distinctLetters(i + 1, res + p(i)) min distinctLetters(i + 1, res + q(i))
      } else {
        res.toSet.size
      }
    }
    val count = distinctLetters(0, "")

    count
  }
}
