import scala.collection.mutable

object OddNetwork {

  def solution(a: Array[Int], b: Array[Int]): Int = {
    val edges = a.zip(b)
    val numOfEdges = edges.length
    val numOfVertices = numOfEdges + 1

    val graph = mutable.Map[Int, Set[Int]]()

    for (edge <- edges) {
      graph.update(edge._1, graph.getOrElse(edge._1, Set[Int]()) + edge._2)
      graph.update(edge._2, graph.getOrElse(edge._2, Set[Int]()) + edge._1)
    }

    val queue = mutable.Queue[(Int, Int)]()
    val visited = mutable.Map[Int, Int]()
    queue.enqueue((0, 0))

    while (queue.nonEmpty) {
      val (node, cost) = queue.dequeue()
      visited(node) = cost
      for (next <- graph.getOrElse(node, Set()).filter(!visited.isDefinedAt(_))) {
        queue.enqueue((next, cost + 1))
      }
    }

    var left = 0
    var right = 0

    for (v <- 0 until numOfVertices) {
      if (visited(v) % 2 == 0) left += 1
      else right += 1
    }

    left * right
  }
}
