object ScooterRoad {

  private val ASPHALT_ON_FOOT = 20
  private val SAND_ON_FOOT = 30
  private val ASPHALT_ON_SCOOTER = 5
  private val SAND_ON_SCOOTER = 40

  private val ASPHALT = 'A'
  private val SAND = 'S'

  def solution(r: String): Int = {
    var min = Int.MaxValue
    var totalOnFoot = 0
    var totalOnScooter = 0

    for (c <- r) {
      val isAsphalt = ASPHALT == c
      val onFootCost = if (isAsphalt) ASPHALT_ON_FOOT else SAND_ON_FOOT
      val onScooterCost = if (isAsphalt) ASPHALT_ON_SCOOTER else SAND_ON_SCOOTER

      totalOnFoot += onFootCost
      totalOnScooter += onScooterCost
      min = math.min(totalOnScooter - totalOnFoot, min)
    }

    math.min(totalOnFoot, math.min(totalOnScooter, min + totalOnFoot))
  }
}
