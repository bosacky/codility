import org.scalatest.flatspec.AnyFlatSpec

class TricoloredTowersTest extends AnyFlatSpec {

  private val testData = List(
    (Array("aab", "cab", "baa", "baa"), 3),
    (Array("zzz", "zbz", "zbz", "dgf"), 2),
    (Array("abc", "cba", "cab", "bac", "bca"), 3))

  for (td <- testData.zipWithIndex) {
    it should (s"Pass example ${td._2 + 1}") in {
      val (input, expected) = td._1
      val actual = TricoloredTowers.solution(input)
      assert(actual === expected)
    }
  }

}
