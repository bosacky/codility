import org.scalatest.flatspec.AnyFlatSpec

class PrettyTilingTest extends AnyFlatSpec {

  private val testData = List(
    (Array("RGBW", "GBRW"), 1),
    (Array("WBGR", "WBGR", "WRGB", "WRGB", "RBGW"), 4),
    (Array("RBGW", "GBRW", "RWGB", "GBRW"), 2),
    (Array("GBRW", "RBGW", "BWGR", "BRGW"), 2)
  )

  for (td <- testData.zipWithIndex) {
    it should (s"Pass example ${td._2 + 1}") in {
      val (input, expected) = td._1
      val actual = PrettyTiling.solution(input)
      assert(actual === expected)
    }
  }

}
