import org.scalatest.propspec.AnyPropSpec

class LetterCoverTest extends AnyPropSpec {
  private val testData = List(
    ("abc", "bcd", 2),
    ("axxz", "yzwy", 2),
    ("bacad", "abada", 1),
    ("amz", "amz", 3)
  )

  property("should find the number of distinct letters") {
    assert(testData.forall { case (a, b, expected) =>
      LetterCover.solution(a, b) == expected
    })
  }
}
