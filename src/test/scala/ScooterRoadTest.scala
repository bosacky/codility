import org.scalatest.flatspec.AnyFlatSpec

class ScooterRoadTest extends AnyFlatSpec {

  private val testData: List[(String, Int)] = List(
    ("ASAASS", 115),
    ("SSA", 80),
    ("SSSSAAA", 175)
  )

  for (td <- testData.zipWithIndex) {
    it should (s"Pass example ${td._2 + 1}") in {
      val (road, expected) = td._1
      val actual = ScooterRoad.solution(road)
      assert(actual === expected)
    }
  }

}

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.time.Span
import org.scalatest.time.SpanSugar._

import scala.language.postfixOps
import scala.util.Random

class ScooterRoadPerfTest extends AnyFlatSpec with TimeLimitedTests {

  override def timeLimit: Span = 10 seconds

  it should "Complete on time" in {
    for (_ <- 1 to 1000) {
      val n = 100000
      val r = (0 until n).map(_ => Random.nextBoolean()).map(if (_) 'A' else 'S').mkString
      val result = ScooterRoad.solution(r)
      assert(result > 0)
    }
  }

  override val defaultTestSignaler: Signaler = (testThread: Thread) => {
    testThread.stop() // deprecated. unsafe. do not use
  }

}

