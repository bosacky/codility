import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.time.Span
import org.scalatest.time.SpanSugar._

import scala.language.postfixOps
import scala.util.Random

class PetsAndToysTest extends AnyFlatSpec{

  it should "Given:\n\n    " +
    "P = [1, 1, 2]\n  " +
    "T = [2, 1, 1]\n  " +
    "A = [0, 2]\n  " +
    "B = [1, 1]\n  " +
    "the function should return true. Person 0 can exchange toys with person 1 to obtain a dog-toy, and then person 1 can exchange toys with person 2." in {
    val pets = Array[Int](1, 1, 2)
    val toys = Array[Int](2, 1, 1)
    val relationFrom = Array[Int](0, 2)
    val relationTo = Array[Int](1, 1)

    val result = PetsAndToys.solution(pets, toys, relationFrom, relationTo)
    assert(result === true)
  }

  it should "Given:\n\n    " +
    "P = [2, 2, 1, 1, 1]\n  " +
    "T = [1, 1, 1, 2, 2]\n  " +
    "A = [0, 1, 2, 3]\n  " +
    "B = [1, 2, 0, 4]\n  " +
    "the function should return false. There is no way for persons 3 and 4 to exchange toys with others." in {
    val pets = Array[Int](2, 2, 1, 1, 1)
    val toys = Array[Int](1, 1, 1, 2, 2)
    val relationFrom = Array[Int](0, 1, 2, 3)
    val relationTo = Array[Int](1, 2, 0, 4)

    val result = PetsAndToys.solution(pets, toys, relationFrom, relationTo)
    assert(result === false)
  }

  it should "Given:\n\n    " +
    "P = [1, 1, 2, 2, 1, 1, 2, 2]\n  " +
    "T = [1, 1, 1, 1, 2, 2, 2, 2]\n  " +
    "A = [0, 2, 4, 6]\n  " +
    "B = [1, 3, 5, 7]\n  " +
    "the function should return false. There is no way for persons 2 and 3 and for persons 4 and 5 to exchange toys with others." in {
    val pets = Array[Int](1, 1, 2, 2, 1, 1, 2, 2)
    val toys = Array[Int](1, 1, 1, 1, 2, 2, 2, 2)
    val relationFrom = Array[Int](0, 2, 4, 6)
    val relationTo = Array[Int](1, 3, 5, 7)

    val result = PetsAndToys.solution(pets, toys, relationFrom, relationTo)
    assert(result === false)
  }

  it should "Given:\n\n    " +
    "P = [2, 2, 2, 2, 1, 1, 1, 1]\n  " +
    "T = [1, 1, 1, 1, 2, 2, 2, 2]\n  " +
    "A = [0, 1, 2, 3, 4, 5, 6]\n  " +
    "B = [1, 2, 3, 4, 5, 6, 7]\n  " +
    "the function should return true." in {
    val pets = Array[Int](2, 2, 2, 2, 1, 1, 1, 1)
    val toys = Array[Int](1, 1, 1, 1, 2, 2, 2, 2)
    val relationFrom = Array[Int](0, 1, 2, 3, 4, 5, 6)
    val relationTo = Array[Int](1, 2, 3, 4, 5, 6, 7)

    val result = PetsAndToys.solution(pets, toys, relationFrom, relationTo)
    assert(result === true)
  }

  it should "Given:\n\n    " +
    "no one knows no one\n" +
    "the function should return false." in {
    val pets = Array[Int](2, 2, 2, 2, 1, 1, 1, 1)
    val toys = Array[Int](1, 1, 1, 1, 2, 2, 2, 2)
    val relationFrom = Array[Int]()
    val relationTo = Array[Int]()

    val result = PetsAndToys.solution(pets, toys, relationFrom, relationTo)
    assert(result === false)
  }

}

class PetsAndToysPerfTest extends AnyFlatSpec with TimeLimitedTests {

  override def timeLimit: Span = 1 seconds

  it should "Complete on time" in {
    val n = 100000
    val m = 200000

    val pets = (0 until n).map(_ => Random.nextInt(2) + 1).toArray
    val toys = (0 until n).map(_ => Random.nextInt(2) + 1).toArray
    val relationFrom = (0 until m).map(_ => Random.nextInt(n)).toArray
    val relationTo = (0 until m).map(_ => Random.nextInt(n)).toArray

    val result = PetsAndToys.solution(pets, toys, relationFrom, relationTo)
    assert(result === false)
  }

  override val defaultTestSignaler: Signaler = (testThread: Thread) => {
    testThread.stop() // deprecated. unsafe. do not use
  }

}