import org.scalatest.funsuite.AnyFunSuite

import scala.util.Random

class DreamTeamTest extends AnyFunSuite {

	test("test 1") {
		val a = Array(4, 2, 1)
		val b = Array(2, 5, 3)
		val f = 2

		val result = DreamTeam.solution(a, b, f)
		assert(result === 10)
	}

	test("test 2") {
		val a = Array(7, 1, 4, 4)
		val b = Array(5, 3, 4, 3)
		val f = 2

		val result = DreamTeam.solution(a, b, f)
		assert(result === 18)
	}

	test("test 3") {
		val a = Array(5, 5, 5)
		val b = Array(5, 5, 5)
		val f = 1

		val result = DreamTeam.solution(a, b, f)
		assert(result === 15)
	}

	test("Max range test") {
		val a = (1 to 200000).map(_ => Random.nextInt(1000)).toArray
		val b = (1 to 200000).map(_ => Random.nextInt(1000)).toArray
		val f = Random.nextInt(200000)

		val result = DreamTeam.solution(a, b, f)
		//TODO: add some threshold
	}

	test("No frontend developers") {
		val result = DreamTeam.solution(Array(1), Array(3), 0)
		assert(result === 3)
	}

	test("Everybody is a frontend developer") {
		val result = DreamTeam.solution(Array(1), Array(3), 1)
		assert(result === 1)
	}

	test("Some other test") {
		val frontEnds = Array(1, 7, 9, 5, 1, 5, 4, 6, 5, 7, 5)
		val backEnds = Array(1, 7, 6, 0, 1, 6, 0, 7, 8, 1, 4)

		val result = DreamTeam.solution(frontEnds, backEnds, 9)
		assert(result === 59)
	}


	test("Different Array sizes") {
		val a = Array(1, 2)
		val b = Array(1, 2, 3)
		intercept[IllegalArgumentException] {
			DreamTeam.solution(a, b, 1)
		}
	}

	test("frontend count greater than array size") {
		val a = Array(1, 2)
		val b = Array(1, 2)
		intercept[IllegalArgumentException] {
			DreamTeam.solution(a, b, 5)
		}
	}
}
