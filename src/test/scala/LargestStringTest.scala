import org.scalatest.flatspec.AnyFlatSpec

class LargestStringTest extends AnyFlatSpec {


  it should "Given S = 'ababb', your function should return 'baaaa'." in {
    val s = "ababb"

    val result = LargestString.solution(s)
    assert(result === "baaaa")
  }

  it should "Given S = 'abbbabb', your function should return 'babaaaa'." in {
    val s = "abbbabb"

    val result = LargestString.solution(s)
    assert(result === "babaaaa")
  }

  it should "Given S = 'aaabab', your function should return 'aaabab'" in {
    val s = "aaabab"

    val result = LargestString.solution(s)
    assert(result === "aaabab")
  }
}

import org.scalatest.concurrent.{Signaler, TimeLimitedTests}
import org.scalatest.time.Span
import org.scalatest.time.SpanSugar._

import scala.language.postfixOps
import scala.util.Random

class LargestStringPerfTest extends AnyFlatSpec with TimeLimitedTests {

  override def timeLimit: Span = 5 seconds

  it should "Complete on time" in {
    //N is an integer within the range [1..100,000];
    //string S consists only of the characters "a" and/or "b".
    val n = 100000
    val s = (0 until n).map(_ => if (Random.nextBoolean) 'a' else 'b').toString()
    val result = LargestString.solution(s)
  }

  it should "Complete on time if everything matches" in {
    val n = 100000
    val s = "abb".repeat(n)
    val result = LargestString.solution(s)
  }

  override val defaultTestSignaler: Signaler = (testThread: Thread) => {
    testThread.stop() // deprecated. unsafe. do not use
  }

}
