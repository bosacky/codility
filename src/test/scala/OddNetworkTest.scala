import org.scalatest.flatspec.AnyFlatSpec

class OddNetworkTest extends AnyFlatSpec {

  private val testData: List[(Array[Int], Array[Int], Int)] = List(
    (Array(), Array(), 0),
    (Array(0, 3, 4, 2, 6, 3), Array(3, 1, 3, 3, 3, 5), 6),
    (Array(0, 4, 2, 2, 4), Array(1, 3, 1, 3, 5), 9),
    (Array(0, 4, 4, 2, 7, 6, 3), Array(3, 5, 1, 3, 4, 3, 4), 16)
  )

  for (td <- testData.zipWithIndex) {
    it should (s"Pass example ${td._2 + 1}") in {
      val (a, b, expected) = td._1
      val actual = OddNetwork.solution(a, b)
      assert(actual === expected)
    }
  }
}
