import org.scalatest.flatspec.AnyFlatSpec

class LargestBalancedRadiusTest extends AnyFlatSpec {
  private val testData: List[(Array[Int], Array[Int], String, Int)] = List(
    (Array(4, 0, 2, -2), Array(4, 1, 2, -3), "RGRR", 2),
    (Array(1, 1, -1, -1), Array(1, -1, 1, -1), "RGRG", 4),
    (Array(1, 0, 0), Array(0, 1, -1), "GGR", 0),
    (Array(5, -5, 5), Array(1, -1, -3), "GRG", 2),
    (Array(3000, -3000, 4100, -4100, -3000), Array(5000, -5000, 4100, -4100, 5000), "RRGRG", 2)
  )

  for (td <- testData.zipWithIndex) {
    it should (s"Pass example ${td._2 + 1}") in {
      val (x, y, colors, expected) = td._1
      val actual = LargestBalancedRadius.solution(x, y, colors)
      assert(actual === expected)
    }
  }
}
