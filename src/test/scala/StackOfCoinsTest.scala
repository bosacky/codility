import org.scalatest.flatspec.AnyFlatSpec

class StackOfCoinsTest extends AnyFlatSpec {

  private val testData = List(
    (Array(2, 3, 1, 3), 5),
    (Array(3, 7, 0, 5), 9),
    (Array(1, 1, 1, 1, 1), 1),
    (Array(1000000), 1000000))

  for (td <- testData.zipWithIndex) {
    it should (s"Pass example ${td._2 + 1}") in {
      val (input, expected) = td._1
      val actual = StackOfCoins.solution(input)
      assert(actual === expected)
    }
  }
}
