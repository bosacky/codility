import org.scalatest.propspec.AnyPropSpec

class DinnerTest extends AnyPropSpec {
  private val testData = List(
    (Array(1, 3, 5, 2, 8, 7), Array(7, 1, 9, 8, 5, 7), 2),
    (Array(1, 1, 1, 1), Array(1, 2, 3, 4), -1),
    (Array(3, 5, 0, 2, 4), Array(1, 3, 10, 6, 7), 0)
  )
  property("should calculate minimum rotation") {
    assert(testData.forall { case (a, b, expected) =>
      Dinner.solution(a, b) == expected
    })
  }

  //TODO: add performance test
  /*
  N is an integer within the range [1..300];
  each element of arrays A and B is an integer within the range [0..1,000];
  arrays A and B have equal sizes.
   */
}
