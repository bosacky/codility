import org.scalatest.flatspec.AnyFlatSpec

class RectangleStripTest extends AnyFlatSpec {

	it should "Given A = [2, 3, 2, 3, 5] and B = [3, 4, 2, 4, 2], the function should return 3" in {
		val a = Array[Int](2, 3, 2, 3, 5)
		val b = Array[Int](3, 4, 2, 4, 2)

		val result = RectangleStrip.solution(a, b)
		assert(result === 3)
	}

	it should "Given A = [2, 3, 1, 3] and B = [2, 3, 1, 3], the function should return 2" in {
		val a = Array[Int](2, 3, 1, 3)
		val b = Array[Int](2, 3, 1, 3)

		val result = RectangleStrip.solution(a, b)
		assert(result === 2)
	}

	it should "Given A = [2, 10, 4, 1, 4] and B = [4, 1, 2, 2, 5], the function should return 3" in {
		val a = Array[Int](2, 10, 4, 1, 4)
		val b = Array[Int](4, 1, 2, 2, 5)

		val result = RectangleStrip.solution(a, b)
		assert(result === 3)
	}

}
